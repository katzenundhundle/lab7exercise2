library string_util;
import 'package:diacritic/diacritic.dart';


// /// A Calculator.
// class Calculator {
//   /// Returns [value] plus 1.
//   int addOne(int value) => value + 1;
// }

class StringUtil{
  int countWord(String str){
    return str.trim()==""? 0: str.trim().split(RegExp(" +")).length;
  }

  //xoa dau trong tieng viet
  String removeAccentFromString(str){
  //   var withDia = 'ĂẶẴẲẮẰẦẤẨẪẬÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
  //   var withoutDia = 'AAAAAAAAAAAAAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz'; 

  //   for (int i = 0; i < withDia.length; i++) {      
  //     str = str.replaceAll(withDia[i], withoutDia[i]);
  //   }

  //   return str;
  
  return removeDiacritics(str);
  }
}