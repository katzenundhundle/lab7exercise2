import 'package:flutter_test/flutter_test.dart';

import 'package:string_util/string_util.dart';

void main() {
  test('count word of input string', () {
    StringUtil stringUtil = StringUtil();
    expect(1,stringUtil.countWord("a"));
    expect(0, stringUtil.countWord(""));
    // expect(3, stringUtil.countWord("My name is"));
    //print(stringUtil.countWord("  My  name is 1 a"));
    print(stringUtil.removeAccentFromString("Ê mầy"));
  });
}
